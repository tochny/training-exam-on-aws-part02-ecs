from flask import Flask, render_template, request, jsonify, abort
import platform
import json
import requests
app = Flask(__name__)
#url_ip = "http://169.254.169.254/latest/meta-data/public_ipv4"
url_ecs_meta = "http://169.254.170.2/v2/metadata"
#url_ecs_task = "http://169.254.170.2/v2/stats"

@app.route('/')
def index():
    if requests.get("http://localhost/_check").status_code == 200:
        return render_template('home.html')
    else:
        return render_template('404.html')

@app.route('/_success')
def home():
    return render_template('home.html')

@app.route('/_check')
def check():
    try:
        ecs_meta = json.loads(requests.get(url_ecs_meta, timeout = 5).text)
        #print(requests.get(url_ecs_task).text)
        #print("In ECS:", ecs_meta['Containers'][1]['Name'])
        #print(ecs_meta['Containers'][1]['Name'])
        #print("hi")
        ecs_meta = json.loads(requests.get(url_ecs_meta, timeout = 5).text)
        return jsonify(result=ecs_meta)
    except requests.exceptions.ConnectionError:
        print("Connection refused")
        return abort(404)


if __name__ == "__main__":
    print(platform.uname())
    print(requests.get('https://api.ipify.org').text.encode('utf-8'))
    app.run(host='0.0.0.0', port=80)
