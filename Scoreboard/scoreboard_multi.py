from flask import Flask,render_template,redirect,session,Blueprint, abort
from flask import Response
from flask import request,jsonify
from datetime import datetime
import time
import json
import requests

app = Flask(__name__)
scoreboard_url = "https://mtfzu2q4gg.execute-api.us-east-1.amazonaws.com/default"
default_Item = {
        "userName": "Foo",
        "hintCount": "None",
        "userScore": "None"
    }
data = default_Item

@app.route('/_stuff')
def add_numbers():
    username = session.get('username','username')
    data['userName'] = username
    r = json.loads(requests.request(method='get', url=scoreboard_url, data=json.dumps(data)).text)
    quizType = r['item']['quizType']
    score = str(r['item']['userScore']).encode('utf-8').decode('utf-8')#str(r1.get(username+"_score"),encoding = "utf-8")
    startTime = time.strftime('%Y-%m-%d %H:%M:%S (UTC + 8)',time.gmtime(float(r['item']['startTime']) + 8 * 60 * 60))
    Hint = str(r['item']['Hint'])
    if(r['item']['Data'] == "Null"):
        endpoint = ""
    else:
        endpoint = str(json.loads(r['item']['Data'])['ip'])
    if r['item']['endTime'] == "None":
        endTime = "None"
    else:
        endTime = time.strftime('%Y-%m-%d %H:%M:%S (UTC + 8)',time.gmtime(float(r['item']['endTime']) + 8 * 60 * 60))
    return jsonify(quizType=quizType, score=score ,startTime=startTime, endTime=endTime, hintCount = str(int(r['item']['hintCount'])), Hint = Hint, endpoint = endpoint)

@app.route('/_admin')
def all_score_admin():
    username_all_score={'None', 'None'}
    return jsonify(result=username_all_score)


@app.route('/')
def login():
    return render_template("login.html")


@app.route('/login', methods=['POST'])
def login_1():
    session['username'] = request.form['login']
    session.permanent = True
    username=session.get('username','username')
    data['userName'] = username
    r = json.loads(requests.request(method='get', url=scoreboard_url, data=json.dumps(data)).text)
    if username not in r['item']['userName']:
        data['userScore'] = 0
        data['hintCount'] = 1
        with open("token.log", "r") as data_file:
            data['userToken'] = data_file.read()
        data['quizType'] = 'ECS'
        data['startTime'] = str(time.time())
        data['endTime'] = "None"
        data['Hint'] = "None"
        data['Tag'] = "Working"
        data['Data'] = "Null"
        r = json.loads(requests.request(method='post', url=scoreboard_url, data=json.dumps(data)).text)
        print(r)
    else:
        print(r)
    if(username=='ADMIN'):
        return redirect('/admin')
    return redirect('/index')

@app.route('/admin')
def admin():
    return render_template("admin.html")

@app.route('/index')
def index():
    username = session.get('username','username')

    getResult=""
    getHintCount=data['hintCount']
    username_all=[]
    return render_template("index_multi.html",username=username,getHintCount=getHintCount, len = len(username_all), username_all = username_all)

@app.route('/token', methods=['POST'])
def token():
    username = session.get('username','username')
    data['userName'] = username
    r = json.loads(requests.request(method='get', url=scoreboard_url, data=json.dumps(data)).text)
    text = request.form['inputToken']
    if r['item']['userToken'] == text and r['item']['endTime'] == 'None':
        data['userScore'] = int(r['item']['userScore']) + int(100)
        data['hintCount'] = int(r['item']['hintCount'])
        data['endTime'] = str(time.time())
        data['Hint'] = r['item']['Hint']
        data['Tag']  = "Complete"
        data['Data'] = r['item']['Data']
        print(data)
        r = json.loads(requests.request(method='post', url=scoreboard_url, data=json.dumps(data)).text)
        print(r)
    return redirect('/index')

@app.route('/getHint', methods=['POST'])
def getHint():
    username = session.get('username','username')
    data['userName'] = username
    r = json.loads(requests.request(method='get', url=scoreboard_url, data=json.dumps(data)).text)
    if int(r['item']['hintCount']) > 0 and r['item']['endTime'] == 'None':
        data['userScore'] = int(r['item']['userScore']) - int(10)
        data['hintCount'] = int(r['item']['hintCount']) - 1
        data['endTime'] = r['item']['endTime']
        data['Hint'] = "Try using docker."
        data['Tag']  = r['item']['Tag']
        data['Data'] = r['item']['Data']
        r = json.loads(requests.request(method='post', url=scoreboard_url, data=json.dumps(data)).text)
    return redirect('/index')

@app.route('/endpoint', methods=['POST'])
def endpoint():
    username = session.get('username','username')
    data['userName'] = username
    r = json.loads(requests.request(method='get', url=scoreboard_url, data=json.dumps(data)).text)
    text = request.form['inputEndpoint']
    data['userScore'] = int(r['item']['userScore'])
    data['hintCount'] = int(r['item']['hintCount'])
    data['endTime'] = r['item']['endTime']
    data['Hint'] = r['item']['Hint']
    data['Tag']  = "Working"
    data['Data'] = json.dumps({'ip' : text})
    print(data)
    r = json.loads(requests.request(method='post', url=scoreboard_url, data=json.dumps(data)).text)
    print(r)
    return redirect('/index')

@app.route('/check', methods=['GET'])
def check():
    username = session.get('username','username')
    data['userName'] = username
    try:
        r = json.loads(requests.request(method='get', url=scoreboard_url, data=json.dumps(data), timeout = 5).text)
        endpoint = json.loads(r['item']['Data'])['ip']
        print(endpoint)
        uri = 'http://' + str(endpoint) + '/_check'
        re = requests.request(method='get', url= uri)
    except:
        return render_template('404.html')
    if(re.status_code == 404):
        return render_template('404.html')
    re = json.loads(re.text)
    if(re['result']['Containers'][0]['Name'] != '~internal~ecs~pause'):
        print(re['result']['Containers'][0]['Name'])
    elif(re['result']['Containers'][1]['Name'] != '~internal~ecs~pause'):
        print(re['result']['Containers'][1]['Name'])
    return render_template('index.html')

#def parse_data()

if __name__ == "__main__":
    app.secret_key = "super secret key"
    app.run(host='0.0.0.0', port=80)
