import sys
import json
import secrets
import logging
import boto3
from botocore.exceptions import ClientError


def upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True


def main(args):
    print('Generate Token')
    token = secrets.token_urlsafe()
    print(token)
    with open("token.log", "w") as data_file:
        data_file.write(token)
    with open("./templates/index.html", "r") as html_file:
        newText = html_file.read().replace("$(tokenID)", token)
    with open("./templates/index.html", "w") as html_file:
        html_file.write(newText)

    #bucket = args[1]
    #upload_file("../docs/index/index.html", bucket, "index.html")
    #upload_file("../docs/index/style.css", bucket, "style.css")
    #upload_file("../docs/404/404.html", bucket, "index.html")
    #upload_file("../docs/404/style.css", bucket, "style.css")

if __name__ == '__main__':
    main(sys.argv)
